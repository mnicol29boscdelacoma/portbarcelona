package cat.boscdelacoma.uf4.portbarcelona.model;

public interface Inspeccionable {

    public boolean isValid();
}
