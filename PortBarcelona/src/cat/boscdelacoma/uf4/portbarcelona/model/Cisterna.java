package cat.boscdelacoma.uf4.portbarcelona.model;

public class Cisterna extends Contenidor {

    public Cisterna() {
        super();
    }
    
    public Cisterna(String numSerie, float capacitat, boolean estat) {
        super(numSerie, capacitat, estat);
    }
    
    @Override
    public void addMercaderia(Mercaderia mercaderia) {
        if (getnMercaderies() == 1) {
            throw new IllegalArgumentException("Aquesta cisterna ja conté 1 líquid: no se'n poden afegir més!");
        } else {
            super.addMercaderia(mercaderia);
        }
    }

    /**
     * Obtenir el volum de la cisterna, en litres.
     * @return volumn de la cisterna, en litres.
     */
    @Override
    public float getVolum() {
        return super.getVolum() / 1000f;
    }

    
}
