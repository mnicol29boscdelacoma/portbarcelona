package cat.boscdelacoma.uf4.portbarcelona.model;

public abstract class Contenidor {

    //<editor-fold defaultstate="collapsed" desc="ATRIBUTS DE CLASE">
    public final static int MAX_MERCADERIES = 100;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="ATRIBUTS D'INSTÀNCIA">
    private String numSerie;
    private float capacitat;
    private Mercaderia[] mercaderies;
    private int nMercaderies;
    private float volumOcupat;
    private boolean estat;  // true = obert; false = tancat
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="CONSTRUCTORS">
    public Contenidor() {
        this("", 0, true);
    }

    public Contenidor(String numSerie, float capacitat, boolean estat) {
        this.numSerie = numSerie;
        this.capacitat = capacitat;
        this.mercaderies = new Mercaderia[MAX_MERCADERIES];
        this.nMercaderies = 0;
        this.estat = estat;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="MÈTODES D'INSTÀNCIA">
    //<editor-fold defaultstate="collapsed" desc="GETTERS/SETTERS">
    public String getNumSerie() {
        return numSerie;
    }

    public void setNumSerie(String numSerie) {
        this.numSerie = numSerie;
    }

    public float getCapacitat() {
        return capacitat;
    }

    public void setCapacitat(float capacitat) {
        this.capacitat = capacitat;
    }

    public int getnMercaderies() {
        return nMercaderies;
    }

    public void setnMercaderies(int nMercaderies) {
        this.nMercaderies = nMercaderies;
    }

    public boolean isObert() {
        return estat;
    }

    public void tancar() {
        this.estat = false;
    }
    //</editor-fold>

    public void addMercaderia(Mercaderia mercaderia) {
        if (!this.estat) {
            // no està tancat
            throw new UnsupportedOperationException("El contenidor ja està tancat. No es poden carregar més mercaderies");
        } else if (nMercaderies == MAX_MERCADERIES) {
            // no està ple
            throw new ArrayIndexOutOfBoundsException("Ja no es poden carregar més mercaderies");
        } else if (getVolum() + mercaderia.getVolum() > this.capacitat) {
            // la mercaderia pot cabre dins el contenidor
            throw new IllegalArgumentException("El volum de la mercaderia fa sobrepassar el límit de càrrega del contenidor");
        } else {
            // afegir la nova mercaderia a l'array de mercaderies
            this.mercaderies[this.nMercaderies] = mercaderia;
            this.nMercaderies++;
            // assignar la refereència al contenidor a la mercaderia afegida
            mercaderia.setContenidor(this);
            // actualitzem el volum ocupat
            volumOcupat += mercaderia.getVolum();
        }
    }

    public float getVolum() {
        return volumOcupat;
    }

    public boolean isValid() {
        boolean valid = true;

        if (this instanceof Inspeccionable) {
            valid = this.isValid();
        }
        return valid;
    }

    @Override
    public String toString() {
        return "Contenidor{" + "numSerie=" + numSerie + ", capacitat=" + capacitat + ", mercaderies=" + mercaderies + ", nMercaderies=" + nMercaderies + ", estat=" + estat + '}';
    }
    //</editor-fold>

}
