package cat.boscdelacoma.uf4.portbarcelona.model;

public class Vaixell {

    public static final int MAX_CONTENIDORS = 1000;
    
    private Contenidor[] contenidors;
    private int nContenidors;
    private float volumOcupat;

    public Vaixell() {
        this.contenidors = new Contenidor[MAX_CONTENIDORS];
        this.nContenidors = 0;
        this.volumOcupat = 0;
    }

    public Vaixell(int nContenidors, float volumOcupat) {
        this();
        this.nContenidors = nContenidors;
        this.volumOcupat = volumOcupat;
    }

    
    /**
     * Afegeix un contenidor en el  vaixell.
     * 
     * Pot generar excepcions en cas que no es compleixin les restriccions indicades.
     * @param contenidor el contenidor que es vol afegir.
     */
    public void addContenidor(Contenidor contenidor) throws ArrayIndexOutOfBoundsException, UnsupportedOperationException  {

        if (nContenidors == MAX_CONTENIDORS) {
            // el contenidor està ple
            throw new ArrayIndexOutOfBoundsException("Ja no es poden carregar més contenidors");
        } else if (!contenidor.isValid()) {
            // el contenidor no es pot carregar => inspecció DUANA
            throw new UnsupportedOperationException("El contenidor no es pot carregar: no compleix els requeriments de la DUANA");
        } else if (contenidor.getnMercaderies() == 0) {
            // el contenidor no té mercaderies
            throw new UnsupportedOperationException("No es poden carregar contenidors buits");
        } else if (contenidor.isObert()) {
            // el contenidor està  obert
            throw new UnsupportedOperationException("No es poden carregar contenidors oberts");
        } else {
            this.contenidors[this.nContenidors] = contenidor;
            this.nContenidors++;

            // acumular el volum del contenidor
            this.volumOcupat += contenidor.getVolum();
        }
    }

    /**
     * Obtenir el volum ocupat per les mercaderies dels contenidors del vaixell
     * @return obtenir el volum ocupat per les mercaderies dels contenidors del vaixell
     */
    public float getVolum() {
        return volumOcupat;
    }

    /**
     * Obetnir el contenidor indicat.
     * @param position la posició del contenidor que es vol obtenir
     * @return el contenidor a que s'ha demanat.
     */
    public Contenidor getContenidor(int position) {
        if(position >= 0 && position < nContenidors) {
            return contenidors[position];
        } else {
            throw new ArrayIndexOutOfBoundsException("La posició indicada no és correcta!");
        }
    }

    public int getnContenidors() {
        return nContenidors;
    }
    
    
    /**
     * Descarrega el vaixell
     */
    public void descarregar() {
        this.nContenidors = 0;
        this.volumOcupat = 0;
    }

}
