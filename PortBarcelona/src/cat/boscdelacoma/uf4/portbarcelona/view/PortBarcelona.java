package cat.boscdelacoma.uf4.portbarcelona.view;

import cat.boscdelacoma.uf4.portbarcelona.model.Cisterna;
import cat.boscdelacoma.uf4.portbarcelona.model.Contenidor;
import cat.boscdelacoma.uf4.portbarcelona.model.DryVan;
import cat.boscdelacoma.uf4.portbarcelona.model.Mercaderia;
import cat.boscdelacoma.uf4.portbarcelona.model.Refrigerat;
import cat.boscdelacoma.uf4.portbarcelona.model.Vaixell;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

public class PortBarcelona {

    public static void main(String[] args) {

        Vaixell vaixell = new Vaixell();
        Mercaderia[] mercaderies = new Mercaderia[2];
        mercaderies[0] = new Mercaderia("Teclat", 0.5f, null);
        mercaderies[1] = new Mercaderia("Pneumàtics", 1200, null);

        Mercaderia m1 = new Mercaderia("Gasolina", 2000, null);
        Mercaderia m2 = new Mercaderia("Formatge", 200, null);

        Contenidor c1 = new DryVan("D9873456", 2500, true, "BLAU");
        Contenidor c2 = new Cisterna("C4562345", 2100, true);
        Contenidor c3 = new Refrigerat("R7638493", 1200, true, -9);

        c2.addMercaderia(m1);
        c3.addMercaderia(m2);

        c2.tancar();
        vaixell.addContenidor(c2);
        c3.tancar();
        vaixell.addContenidor(c3);

        System.out.println("AFEGIR MERCADERIES");
        afegirMercaderies(c1, mercaderies);
        System.out.println("-------------------------------------------------");
        System.out.println("CARREGAR CONTENIDOR DRY VAN");
        c1.tancar();
        carregarContenidor(vaixell, c1);
        System.out.println("-------------------------------------------------");
        System.out.println("CARREGAR CONTENIDOR CISTERNA");
        carregarContenidor(vaixell, c2);
        System.out.println("-------------------------------------------------");
        System.out.println("CARREGAR CONTENIDOR REFRIGERAT");
        carregarContenidor(vaixell, c3);
        System.out.println("-------------------------------------------------");
        System.out.println("MOSTRAR CONTENIDORS VAIXELL");
        mostrarContenidors(vaixell);
        System.out.println("-------------------------------------------------");
        System.out.println("DESCARREGAR VAIXELL");
        descarregarVaixell(vaixell);
    }

    static void afegirMercaderies(Contenidor contenidor, Mercaderia[] mercaderies) {
        for (Mercaderia mercaderia : mercaderies) {
            contenidor.addMercaderia(mercaderia);
        }
    }

    static void carregarContenidor(Vaixell vaixell, Contenidor contenidor) {
        vaixell.addContenidor(contenidor);
    }

    static void descarregarVaixell(Vaixell vaixell) {
        LocalDate data = LocalDate.now();
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        System.out.println("PORT DE BARCELONA\n");
        System.out.printf("DATA: %s\n", data.format(format));
        System.out.println("CONTENIDOR         VOLUM");
        System.out.println("------------------------");
        Contenidor contenidor;

        for (int i = 0; i < vaixell.getnContenidors(); i++) {
            contenidor = vaixell.getContenidor(i);
            System.out.printf("%s %12.2f m3\n", contenidor.getNumSerie(), contenidor.getVolum());
        }
        System.out.printf("\nVOLUM TOTAL: %8.2f m3\n", vaixell.getVolum());
        System.out.println();
        vaixell.descarregar();
    }

    static void mostrarContenidors(Vaixell vaixell) {
        Contenidor contenidor;
        long countMercaderies = 0;

        for (int i = 0; i < vaixell.getnContenidors(); i++) {
            contenidor = vaixell.getContenidor(i);
            System.out.printf("%s\n", contenidor.getNumSerie());
            countMercaderies += contenidor.getnMercaderies();
        }
        System.out.printf("\nTOTAL MERCADERIES: %d\n", countMercaderies);
    }

}
